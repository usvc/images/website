# `usvc/images/website`

[![pipeline status](https://gitlab.com/usvc/images/website/badges/master/pipeline.svg)](https://gitlab.com/usvc/images/website/commits/master)
[![dockerhub link](https://img.shields.io/badge/dockerhub-usvc%2Fwebsite-blue)](https://hub.docker.com/r/usvc/website)

This is an nginx image that comes with tooling required for deploying a static site into a cloud native environment. This includes:

- `/metrics` endpoint for Prometheus
- `/healthz` endpoint for liveness checks
- `/readyz` endpoint for readiness checks
- Logs streaming to a defined FluentD instance

# Usage

## As a Base Image

```dockerfile
FROM usvc/website:latest
COPY . /www/html
```

## As an Image

```sh
docker run -it \
  --volume "$(pwd):/www/html" \
  --publish 8080:8080 usvc/website:latest;
```

## Metrics

The metrics endpoint is at `/metrics`.

## Liveness Check

The liveness check is at `/healthz`

By default this returns a HTTP status code of 200. If a file is defined at `/www/sh/liveness/check.sh`, the script will be run. If the script exists, the output of the `/healthz` endpoint will be the output of the script. If the script exists with zero, 200 is sent, otherwise 500 is sent.

The content type is `text/plain`.

## Readiness Check

The readiness check is at `/readyz`.

By default this returns a HTTP status code of 200. If a file is defined at `/www/sh/readiness/check.sh`, the script will be run. If the script exists, the output of the `/healthz` endpoint will be the output of the script. If the script exists with zero, 200 is sent, otherwise 500 is sent.

The content type is `text/plain`.

## Logs Streaming

You can use the following environment variables to configure the location which will receive the FluentD logs:

| Key | Default | Description |
| --- | --- | --- |
| `FLUENTD_HOST` | `nil` | The hostname of the FluentD service |
| `FLUENTD_PORT` | `nil` | The port which the FluentD service is listening on |

When the above environment variables are not specified, streaming to another FluentD is disabled.

## Logs Location

To configure the location which the logs are stored, use the following environment variables:

| Key | Default | Description |
| --- | --- | --- |
| `LOGS_PATH_ACCESS` | `"var/log/nginx/access.log"` | The path relative to `/` of the nginx access logs |
| `LOGS_PATH_ERROR` | `"var/log/nginx/error.log"` | The path relative to `/` of the nginx error logs |

## Pull the Image

```sh
docker pull usvc/website:latest
```

# Development Runbook

## Testing integrations

1. Run `make test_integration` from the root of the repository
2. Ensure that in the display you see `application_w_stream`, `application_wo_stream` and `fluentd` logs.
3. Ensure that the `fluentd` logs belong to `application_w_stream`
4. Visit Prometheus at [http://localhost:9090/targets](http://localhost:9090/targets) and ensure all targets are up
5. Visit Prometheus at [http://localhost:9090/graph](http://localhost:9090/graph) and enter `nginx_http_requests_total` as the Expression and hit Execute. Ensure you see 2 types of `job` labels, with and without streaming.
6. Visit Grafana at [http://localhost:3000](http://localhost:3000), create a new dashboard, select **Prometheus** as the Query source, and enter in `nginx_http_connections` as the Metrics. Ensure that something is showing.
7. Visit Kibana at [http://localhost:5601/app/kibana](http://localhost:5601/app/kibana), add the `fluentd-*` pattern, use `@timestamp` for time filter, and ensure logs are showing at [http://localhost:5601/app/kibana#/discover](http://localhost:5601/app/kibana#/discover).

## CI/CD Variables

| Key | Variable |
| --- | --- |
| `DOCKER_REGISTRY_URL` | URL to the Docker registry |
| `DOCKER_REGISTRY_USER` | Username to login to the Docker registry |
| `DOCKER_REGISTRY_PASSWORD` | Password for the `DOCKERHUB_USERNAME` account |

# License

This code is licensed under [the MIT license](./LICENSE).


# References

Where credit is due!

1. https://stackoverflow.com/questions/4990990/check-if-a-file-exists-with-lua
2. https://serverfault.com/questions/196929/reply-with-200-from-nginx-config-without-serving-a-file
3. https://stackoverflow.com/questions/132397/get-back-the-output-of-os-execute-in-lua
4. https://stackoverflow.com/questions/44098136/nginx-config-with-spa-and-subdirectory-root
