IMAGE_URL=usvc/website
TIMESTAMP=$$(date +'%Y%m%d')

build:
	@docker build -t $(IMAGE_URL):latest .

ci.import:
	docker load --input ./.export/image.tar

ci.export: build
	mkdir -p ./.export
	docker save --output ./.export/image.tar $(IMAGE_URL):latest

test: build
	@container-structure-test test \
	 	--verbosity debug \
	 	--image $(IMAGE_URL):latest \
		--config ./shared/tests/base.yaml

test_integration:
	@docker-compose -f ./deployments/docker-compose.yml up -V

publish: build
	@docker tag $(IMAGE_URL):latest $(IMAGE_URL):$(TIMESTAMP)
	@docker push $(IMAGE_URL):latest
	@docker push $(IMAGE_URL):$(TIMESTAMP)
