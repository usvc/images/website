FROM openresty/openresty:alpine

# specifies the version of the prometheus plugin
ARG NGINX_LUA_PROMETHEUS_VERSION=0.20181120
# specifies the root of the fluentd configuration
ARG FLUENTD_ROOT=etc/fluent
# specifies the path where nginx logs are sent to
ARG NGINX_LOGS_PATH=var/log/nginx
# specifies the openresty nginx configuration path
ARG OPENRESTY_NGINX_PATH=usr/local/openresty/nginx
# specifies where the static website needs to be
ARG SERVER_ROOT=www/html
# specifies where the scripts should be
ARG SERVER_SCRIPTS=www/sh

# due diligence
RUN apk update --no-cache && apk upgrade --no-cache
# add the nginx user
RUN addgroup -S nginx \
  && adduser -H -D -G nginx -u 1000 nginx
# install the prometheus metrics plugin
RUN wget -O /opt/nginx-lua-prometheus.tar.gz \
    https://github.com/knyar/nginx-lua-prometheus/archive/${NGINX_LUA_PROMETHEUS_VERSION}.tar.gz \
  && tar -xvf /opt/nginx-lua-prometheus.tar.gz -C /opt \
  && mv /opt/nginx-lua-prometheus-${NGINX_LUA_PROMETHEUS_VERSION}/ /opt/nginx-lua-prometheus \
  && rm -rf /opt/nginx-lua-prometheus.tar.gz
# install fluentd
RUN apk add --no-cache build-base ruby ruby-dev ruby-etc ruby-irb ruby-rdoc ruby-webrick supervisor \
  && gem install fluentd \
  && gem cleanup \
  && apk del --no-cache build-base ruby-dev
# create the configuration extension directory
RUN mkdir -p \
    /${NGINX_LOGS_PATH} \
    /${FLUENTD_ROOT} \
  && touch \
    /${NGINX_LOGS_PATH}/access.log \
    /${NGINX_LOGS_PATH}/error.log \
  && chown nginx:nginx -R \
    /${NGINX_LOGS_PATH} \
    /${OPENRESTY_NGINX_PATH} \
    /${FLUENTD_ROOT}
COPY --chown=nginx:nginx ./assets/index.html /${SERVER_ROOT}/index.html
COPY --chown=nginx:nginx ./healthchecks /${SERVER_SCRIPTS}
COPY --chown=nginx:nginx ./scripts/ /usr/bin/
COPY --chown=nginx:nginx ./README.md /README.md
COPY --chown=nginx:nginx ./config/nginx.conf /${OPENRESTY_NGINX_PATH}/conf/nginx.conf
COPY --chown=nginx:nginx ./config/fluent /${FLUENTD_ROOT}/
COPY --chown=nginx:nginx ./config/supervisord.conf /etc/supervisord.conf
ENV FLUENT_CONF="/${FLUENTD_ROOT}/fluent.conf" \
  LOGS_PATH_ACCESS="${NGINX_LOGS_PATH}/access.log" \
  LOGS_PATH_ERROR="${NGINX_LOGS_PATH}/error.log"
ENTRYPOINT ["entrypoint.sh"]
VOLUME [ "/${SERVER_ROOT}", "/${SERVER_SCRIPTS}" ]
EXPOSE 8080
LABEL maintainer "@zephinzer [https://gitlab.com/zephinzer]"
LABEL canonical_url "https://gitlab.com/usvc/images/website"
LABEL description "an image for hosting static websites in a cloud-native architecture"
LABEL dockerhub_url "https://hub.docker.com/r/usvc/website"
LABEL usage_as_base "do a COPY of your static site into /${SERVER_ROOT}"
LABEL usage_as_image "mount the volume containing your static site at /${SERVER_ROOT}"
