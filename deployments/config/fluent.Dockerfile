FROM fluent/fluentd:v1.7
USER root
RUN apk add --update --no-cache build-base ruby-dev \
  && fluent-gem install fluent-plugin-prometheus --version=1.5.0 \
  && fluent-gem install fluent-plugin-elasticsearch --version=3.5.5 \
  && gem sources --clear-all \
  && apk del build-base ruby-dev
USER fluent
