import React from 'react';
import './index.css';

export default () => (
  <header className="home">
    <p>
      Welcome
    </p>
    <a
      className="home-link"
      href="https://reactjs.org"
      target="_blank"
      rel="noopener noreferrer"
    >
      Learn React
    </a>
  </header>
)