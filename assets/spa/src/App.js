import React from 'react';
import Home from './Home';
import {LevelOne, LevelTwo, LevelThree} from './Levels';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import './App.css';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/level1" component={LevelOne} exact />
          <Route path="/le/vel2" component={LevelTwo} exact />
          <Route path="/le/vel/3" component={LevelThree} exact />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
