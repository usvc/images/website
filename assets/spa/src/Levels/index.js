import React from 'react';
import './index.css';

export const LevelOne = () => (
  <header className="level">
    <p>
      LevelOne
    </p>
    <a
      className="home-link"
      href="https://reactjs.org"
      target="_blank"
      rel="noopener noreferrer"
    >
      Learn React
    </a>
  </header>
);

export const LevelTwo = () => (
  <header className="level">
    <p>
      LevelTwo
    </p>
    <a
      className="home-link"
      href="https://reactjs.org"
      target="_blank"
      rel="noopener noreferrer"
    >
      Learn React
    </a>
  </header>
);

export const LevelThree = () => (
  <header className="level">
    <p>
      LevelThree
    </p>
    <a
      className="home-link"
      href="https://reactjs.org"
      target="_blank"
      rel="noopener noreferrer"
    >
      Learn React
    </a>
  </header>
);
