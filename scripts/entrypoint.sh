#!/bin/sh
######################################################################################
# this script configures the necessary fluentd configuration depending on whether the
# environment variables FLUENTD_HOST and FLUENTD_PORT are available. this script
# exists because fluentd as of v1.7.0 does not provide a conditional structure to
# handle situations where something is defined or not
######################################################################################

if [ "${FLUENTD_HOST}" = "" ] || [ "${FLUENTD_PORT}" = "" ]; then
  printf -- '[entrypoint] streaming to external fluentd is DISABLED\n';
  cp /etc/fluent/stdout.conf /etc/fluent/fluent.conf;
else 
  printf -- "[entrypoint] streaming to external fluentd is ENABLED at ${FLUENTD_HOST}:${FLUENTD_PORT}\n";
  cp /etc/fluent/forward.conf /etc/fluent/fluent.conf;
fi;

supervisord -c /etc/supervisord.conf;
