#!/bin/sh
######################################################################################
# this script is necessary to trigger fluentd's tailing mechanism which seems to fail
# to start on fluentd v1.7.0. to test if this script is still required, remove the
# call to this script in the supervisord.conf
######################################################################################

bom()
{
  wget \
    "--header=User-Agent: heartbeat" \
    "--header=Referer: heartbeat" \
    -O /dev/null \
    127.0.0.1:8080;
}

SERVER_IS_UP=0;

while [ "${SERVER_IS_UP}" = "0" ]; do
  bom;
  if [ "$?" = "0" ]; then
    SERVER_IS_UP=1;
  fi;
  sleep 0.3;
done;

while :; do
  sleep 60 && bom;
done;
