#!/bin/sh

printf -- "fluentd_$(fluentd --version | cut -f 2 -d ' ')\n";
printf -- "nginx_$(nginx -v &>/dev/stdout | cut -f 2 -d '/')\n";
