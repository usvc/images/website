#!/bin/sh
######################################################################################
# this script kills supervisord and is triggered by supervisord whenever one of the
# main services fail - see the eventhandlers in supervisord.conf for details
######################################################################################

printf "READY\n";

while read line; do
  echo "Processing Event: $line" >&2;
  kill -3 $(cat "/var/run/supervisord.pid")
done < /dev/stdin
